//
//  main.cpp
//  algoritmoGeneticoEDA
//
//  Created by Estefania Chavez Guardado on 2/6/15.
//  Copyright (c) 2015 Estefania Chavez Guardado. All rights reserved.
//

#include <iostream>


int GEN_A = 1;
int GEN_B = 0;
float PROBABILIDAD_DE_MUTACION = 0.6;
float PROBABILIDAD_DE_CRUZAMIENTO = 0.85;

int INDIVIDUOS_DE_POBLACION = 8;
int NUMERO_GENES_POR_CROMOSOMA_MAX = 8;
int NUMERO_DE_GENERACIONES = 10;


int poblacion[8][8]; //numero de individuos por genes
int evaluacionPoblacion[8]; //numero de genes

//poblacion[i][j] = drand48() * (1);;

void generarPoblacionInicial(){
    srand (time(NULL));
    for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i) {
        for (int j = 0 ; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
            poblacion[i][j] = rand() % 2;
        }
    }
    
}

void OneMax(){
    
    for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i) {
        int sumaCromosoma = 0;
        for (int j = 0 ; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
            sumaCromosoma += poblacion[i][j];
        }
        evaluacionPoblacion[i] = sumaCromosoma;
    }
    
}

int poblacionOrdenada[8][8]; //individuos por numero de genes
int elitista[8]; // numero de genes


void asignacionPoblacionOrdenada(){
    for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i) {
        for (int j=0 ; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
            poblacionOrdenada[i][j] = poblacion[i][j];
        }
    }

}

void ordenarPoblacionAscendentemente(){
    
    asignacionPoblacionOrdenada();
    
    for (int i = 1; i < INDIVIDUOS_DE_POBLACION; ++i) {
        for (int j = 0; j <INDIVIDUOS_DE_POBLACION-1; ++j) {
            if (evaluacionPoblacion[j] < evaluacionPoblacion[j+1]) {
                int evaluacion = evaluacionPoblacion[j];
                evaluacionPoblacion[j] = evaluacionPoblacion[j+1];
                evaluacionPoblacion[j+1] = evaluacion;
                
                int temporal[8];
                for (int a = 0; a < NUMERO_GENES_POR_CROMOSOMA_MAX; ++a)
                    temporal[a] = poblacionOrdenada[j][a];
                for (int a = 0; a < NUMERO_GENES_POR_CROMOSOMA_MAX; ++a)
                    poblacionOrdenada[j][a] = poblacionOrdenada[j+1][a];
                for (int a = 0; a < NUMERO_GENES_POR_CROMOSOMA_MAX; ++a)
                    poblacionOrdenada[j+1][a] = temporal[a];
                
            }
        }
    }
}

int obtenerElitista(){
    
    for (int a = 0; a < NUMERO_GENES_POR_CROMOSOMA_MAX; ++a)
        elitista[a] = poblacionOrdenada[0][a];
    
    int valor = evaluacionPoblacion[0];
    
    return valor;
}

int generacion[8][8];

void padresYElitista(){
    for (int a = 0 ; a < NUMERO_GENES_POR_CROMOSOMA_MAX; ++a) {
        generacion[0][a] = elitista[a];
        generacion[1][a]=poblacionOrdenada[1][a];
        generacion[2][a]=poblacionOrdenada[1][a];
    }
}

void cruzamiento(){
    srand48(time(NULL));
    float cruzamiento = 0.5;
    int cromosomaCruzado[8];
    
    for (int i=3; i < INDIVIDUOS_DE_POBLACION; ++i) {
        float probabilidad = drand48() * 1;
        int primer = i-2;
        int segundo = i-1;

        if (cruzamiento > probabilidad) {
            int puntoDeCruzamiento = drand48() * NUMERO_GENES_POR_CROMOSOMA_MAX;
            int j=0;
            while (j <= puntoDeCruzamiento) {
                cromosomaCruzado[j] =generacion[primer][j];
                ++j;
            }
            while (j <=NUMERO_GENES_POR_CROMOSOMA_MAX) {
                cromosomaCruzado[j] =generacion[segundo][j];
                ++j;
            }
            
            for (int  a = 0; a < INDIVIDUOS_DE_POBLACION; ++a)
                generacion[i][a] = cromosomaCruzado[a];
        } else{
            for (int a = 0 ; a < INDIVIDUOS_DE_POBLACION; ++a)
                generacion[i][a]= generacion[primer][a];
            
        }
    }
}

void mutacion(){
    srand48(time(NULL));
    float mutacion = 0.5;

    for (int i=0; i < INDIVIDUOS_DE_POBLACION; ++i) {
        for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
            float probabilidad = drand48() * 1;
            
            if (mutacion > probabilidad) {
                if (generacion[i][j] == 0)
                    generacion[i][j] = 1;
                else
                    generacion[i][j] = 0;
            }
        }
    }
    
}

void nuevaPoblacion(){
    padresYElitista();
    cruzamiento();
    mutacion();
    
    for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i) {
        for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
            poblacionOrdenada[i][j] = generacion[i][j];
        }
    }
}

void AG(){
    int generacion = 0;
    std::cout << "\t ALGORITMO GENETICO";

    do{

        std::cout << "Generacion "<< generacion <<"\n      Cromosoma OneMax \n";
        for (int i = 0 ; i < INDIVIDUOS_DE_POBLACION; ++i) {
            std::cout << "      ";
            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
                std::cout << poblacionOrdenada[i][j];
            }
            std::cout << "    " << evaluacionPoblacion[i] << "\n";
        }
        
        int pesoElitista = obtenerElitista();
        std::cout << "\nElitista\n";
        for (int a = 0; a < NUMERO_GENES_POR_CROMOSOMA_MAX; ++a)
            std::cout << elitista[a];
        std::cout << "\nOneMax elitista: " << pesoElitista << "\n";
        
        nuevaPoblacion();
        
        std::cout << "\n";
        
        ++generacion;
    }while (generacion <= 10);
    
    
}

int evaluacionDeVariablesColumnas[8]; //numero de variables (genes) (suma de unos por columna)

void evaluacionPorVariableColumna(){
    
    for (int i = 0; i < NUMERO_GENES_POR_CROMOSOMA_MAX; ++i) {
        int sumaCromosoma = 0;
        for (int j = 0 ; j < INDIVIDUOS_DE_POBLACION; ++j) {
            sumaCromosoma += poblacionOrdenada[j][i];
        }
        evaluacionDeVariablesColumnas[i] = sumaCromosoma;
    }
}

float estimacionDeProbabilidad[8]; // numero de variables

void frecuenciaPorVariable(){
    for (int a = 0; a < NUMERO_GENES_POR_CROMOSOMA_MAX; ++a) {
        estimacionDeProbabilidad[a] = (float)evaluacionDeVariablesColumnas[a]/ (float)INDIVIDUOS_DE_POBLACION;
    }
}

void comparacionRandomFrecuenciasPorVariable(){
    int poblacionDistribucion[8][8];
    srand48(time(NULL));

    for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i) {
        for (int j = 0 ; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
            float random = (drand48() * 1) + 0.1;
            if (random < estimacionDeProbabilidad[j])
                poblacionDistribucion[i][j] = 0;
            else if (random > estimacionDeProbabilidad[j])
                poblacionDistribucion[i][j] = 1;
        }
    }
    
    for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i)
        for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j)
            poblacion[i][j] = poblacionDistribucion[i][j];
        
    
}

void evaluacionPorVariables(){
    evaluacionPorVariableColumna();
    frecuenciaPorVariable();
    comparacionRandomFrecuenciasPorVariable();
}

bool banderaDeParada;

void EDA(){
    banderaDeParada = false;
    generarPoblacionInicial();
    int generacion = 0;
    std::cout << "\t ALGORITMO EDA";

    do{

        OneMax();
        ordenarPoblacionAscendentemente();
        std::cout << "Generacion "<< generacion <<"\n      Cromosoma OneMax \n";
        for (int i = 0 ; i < INDIVIDUOS_DE_POBLACION; ++i) {
            std::cout << "      ";
            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
                std::cout << poblacionOrdenada[i][j];
            }
            std::cout << "    " << evaluacionPoblacion[i] << "\n";
        }
        
        std::cout << "Probabilidades: \n";
        for (int i = 0; i < NUMERO_GENES_POR_CROMOSOMA_MAX; ++i) {
            std::cout << "X" <<i << "= " << estimacionDeProbabilidad[i] << " ";
        }
        
        int pesoElitista = obtenerElitista();
        std::cout << "\nElitista\n";
        for (int a = 0; a < NUMERO_GENES_POR_CROMOSOMA_MAX; ++a)
            std::cout << elitista[a];
        std::cout << "\nOneMax elitista: " << pesoElitista << "\n";
        
        evaluacionPorVariables();
    
        if (evaluacionPoblacion[0] == NUMERO_GENES_POR_CROMOSOMA_MAX)
            banderaDeParada = true;
        
        std::cout << "\n";
        
        ++generacion;
    }while (generacion <= 10);
    
}


int main(int argc, const char * argv[]) {
    EDA();
    AG();
    
    return 0;
}
